<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt($faker->password),        
        'remember_token' => str_random(10),
        //'password' => $faker->unique()->password, // secret
        //'password' => $faker->unique()->password, // secret
        
    ];
    $this->assertFalse(Auth::check());

    $this->visit( route('login') )
    ->submitForm('Login', [
        'email' => "panda@animal.com",
        'password' => "bamboo"
    ])
    ->see('Welcome')
    ->seePageIs( route('app') );

    $this->assertTrue(Auth::check());
});
